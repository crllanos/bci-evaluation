package cl.bci.crllanos.evaluation.service.impl;

import cl.bci.crllanos.evaluation.repository.UserRegistryRepository;
import cl.bci.crllanos.evaluation.repository.entity.UserEntity;
import cl.bci.crllanos.evaluation.service.IUserRegistryService;
import cl.bci.crllanos.evaluation.service.bo.UserBO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class UserRegistryService implements IUserRegistryService {

	Logger logger = LoggerFactory.getLogger(UserRegistryService.class);

	@Autowired
	private UserRegistryRepository repository;

	private static final Pattern VALID_EMAIL_ADDRESS_REGEX =
			Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
	private static final Pattern VALID_PASSWORD_REGEX =
			Pattern.compile("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$)$");

	@Override
	public String saveUser(UserEntity user) throws Exception {
		logger.info(String.format("UserRegistryService.saveUser(%s)", user));

		// Validates password
		if(StringUtils.isEmpty(user.getPassword())){ //  || !validatePassword(user.getPassword()) // @todo error en el regex
			throw new Exception("Invalid password");
		}

		// validates email
		if(StringUtils.isEmpty(user.getEmail()) || !validateEmail(user.getEmail())){
			throw new Exception("Invalid e-mail");
		}

		// todo validar email unique: "El correo ya registrado"
		// if...

		return repository.save(user);
	}

	@Override
	public UserBO findUserById(String id) throws Exception {
		UserEntity userEntity = repository.findById(id);
		if(userEntity == null) throw new Exception(String.format("User ID %s not found", id));
		UserBO userBO = new UserBO();
		userBO.setId(userEntity.getId());
		userBO.setCreated(userEntity.getCreated());
		userBO.setModified(userEntity.getModified());
		userBO.setLastLogin(userEntity.getLastLogin());
		userBO.setToken(userEntity.getToken());
		userBO.setActive(userEntity.isActive());
		return userBO;
	}

	@Override
	public List<UserBO> findAllUsers() {
		List<UserBO> response = new ArrayList<>();
		for(Map.Entry<String, UserEntity> entry : repository.findAll().entrySet()) {
			logger.info(String.format("Entry key: <%s>, entry value <%s> ", entry.getKey(), entry.getValue()));
			UserBO userBO = new UserBO();
			UserEntity userEntity = (UserEntity) entry.getValue();
			userBO.setId(userEntity.getId());
			userBO.setCreated(userEntity.getCreated());
			userBO.setModified(userEntity.getModified());
			userBO.setLastLogin(userEntity.getLastLogin());
			userBO.setToken(userEntity.getToken());
			userBO.setActive(userEntity.isActive());

			response.add(userBO);
		}
		return response;
	}

	private static boolean validateEmail(String emailStr) {
		Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr);
		return matcher.find();
	}

	private static boolean validatePassword(String pwd) {
		Matcher matcher = VALID_PASSWORD_REGEX.matcher(pwd);
		return matcher.find();
	}
}
