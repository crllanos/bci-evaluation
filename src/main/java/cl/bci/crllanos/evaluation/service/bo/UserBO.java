package cl.bci.crllanos.evaluation.service.bo;

import java.util.Date;

public class UserBO {
	private String id;
	private Date created;
	private Date modified;
	private Date lastLogin;
	private String token;
	private boolean active;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getModified() {
		return modified;
	}

	public void setModified(Date modified) {
		this.modified = modified;
	}

	public Date getLastLogin() {
		return lastLogin;
	}

	public void setLastLogin(Date lastLogin) {
		this.lastLogin = lastLogin;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("UserBO{");
		sb.append("id='").append(id).append('\'');
		sb.append(", created=").append(created);
		sb.append(", modified=").append(modified);
		sb.append(", lastLogin=").append(lastLogin);
		//sb.append(", token='").append(token).append('\'');
		sb.append(", active=").append(active);
		sb.append('}');
		return sb.toString();
	}
}
