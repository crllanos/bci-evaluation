package cl.bci.crllanos.evaluation.service;

import cl.bci.crllanos.evaluation.repository.entity.UserEntity;
import cl.bci.crllanos.evaluation.service.bo.UserBO;

import java.util.List;

public interface IUserRegistryService {
	public String saveUser(UserEntity user) throws Exception;
	public UserBO findUserById(String id) throws Exception;
	public List<UserBO> findAllUsers();
}
