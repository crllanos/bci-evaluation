package cl.bci.crllanos.evaluation.repository.entity;

import java.util.Date;
import java.util.List;

public class UserEntity{
	private String id;
	private String name;
	private String email;
	private String password;
	private Date created;
	private Date modified;
	private Date lastLogin;
	private String token;
	private boolean active;
	private List<PhoneEntity> phones;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getModified() {
		return modified;
	}

	public void setModified(Date modified) {
		this.modified = modified;
	}

	public Date getLastLogin() {
		return lastLogin;
	}

	public void setLastLogin(Date lastLogin) {
		this.lastLogin = lastLogin;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public List<PhoneEntity> getPhones() {
		return phones;
	}

	public void setPhones(List<PhoneEntity> phones) {
		this.phones = phones;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("UserEntity{");
		sb.append("id='").append(id).append('\'');
		sb.append(", name='").append(name).append('\'');
		sb.append(", email='").append(email).append('\'');
		//sb.append(", password='").append(password).append('\'');
		sb.append(", created=").append(created);
		sb.append(", modified=").append(modified);
		sb.append(", lastLogin=").append(lastLogin);
		//sb.append(", token='").append(token).append('\'');
		sb.append(", active=").append(active);
		sb.append(", phones=").append(phones);
		sb.append('}');
		return sb.toString();
	}
}
