package cl.bci.crllanos.evaluation.repository;

import cl.bci.crllanos.evaluation.repository.entity.UserEntity;

import java.util.Map;

public interface RedisRepository {

	String save(UserEntity user);

	UserEntity findById(String id);

	Map<String, UserEntity> findAll();

}
