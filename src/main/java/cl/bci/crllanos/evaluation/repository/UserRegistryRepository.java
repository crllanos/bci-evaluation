package cl.bci.crllanos.evaluation.repository;

import cl.bci.crllanos.evaluation.repository.entity.UserEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.Date;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

@Repository
public class UserRegistryRepository implements RedisRepository {

	Logger logger = LoggerFactory.getLogger(UserRegistryRepository.class);

	@Value("${app.redis.user-key}")
	private String userKey;

	@Autowired
	private RedisTemplate<String, UserEntity> redisTemplate;

	private HashOperations hashOperations;
	@PostConstruct
	private void init(){
		hashOperations = redisTemplate.opsForHash();
	}

	@Override
	public String save(UserEntity user) {
		logger.info(String.format("Saving user %s, key <%s> ", user, this.userKey));
		String uuid = UUID.randomUUID().toString();
		user.setId(uuid);
		user.setCreated(new Date());
		user.setModified(new Date());
		user.setLastLogin(new Date());
		user.setToken(user.getId()); // @todo Cambiar por jwt
		// para efectos del ejemplo, el usuario quedará activo de manera aleatoria
		int randomNum = ThreadLocalRandom.current().nextInt(1, 100);
		user.setActive(randomNum %2 == 0);
		hashOperations.put(this.userKey, uuid, user);
		return uuid;
	}

	@Override
	public UserEntity findById(String id) {
		return (UserEntity) hashOperations.get(this.userKey, id);
	}

	@Override
	public Map<String, UserEntity> findAll() {
		return hashOperations.entries(this.userKey);
	}
}
