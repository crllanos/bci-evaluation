package cl.bci.crllanos.evaluation.repository.entity;

public class PhoneEntity{
	private Integer id;
	private Integer number;
	private Integer citycode;
	private Integer contrycode;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	public Integer getCitycode() {
		return citycode;
	}

	public void setCitycode(Integer citycode) {
		this.citycode = citycode;
	}

	public Integer getContrycode() {
		return contrycode;
	}

	public void setContrycode(Integer contrycode) {
		this.contrycode = contrycode;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("PhoneEntity{");
		sb.append("id=").append(id);
		sb.append(", number=").append(number);
		sb.append(", citycode=").append(citycode);
		sb.append(", contrycode=").append(contrycode);
		sb.append('}');
		return sb.toString();
	}
}
