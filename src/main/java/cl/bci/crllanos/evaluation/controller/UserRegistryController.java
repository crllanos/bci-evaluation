package cl.bci.crllanos.evaluation.controller;

import cl.bci.crllanos.evaluation.controller.dto.PhoneDTO;
import cl.bci.crllanos.evaluation.controller.dto.RegistryResponseDTO;
import cl.bci.crllanos.evaluation.controller.dto.UserRegistryRequestDTO;
import cl.bci.crllanos.evaluation.controller.dto.UserRegistryResponseDTO;
import cl.bci.crllanos.evaluation.repository.entity.PhoneEntity;
import cl.bci.crllanos.evaluation.repository.entity.UserEntity;
import cl.bci.crllanos.evaluation.service.IUserRegistryService;
import cl.bci.crllanos.evaluation.service.bo.UserBO;
import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
public class UserRegistryController {

	Logger logger = LoggerFactory.getLogger(UserRegistryController.class);

	@Value("${app.secret}")
	private String secret;

	@Autowired
	IUserRegistryService iUserRegistryService;

	@GetMapping("/ping")
	public String ping(){
		logger.info("pong!");
		return "pong! " + new java.util.Date();
	}

	@PostMapping("/user-registry")
	public ResponseEntity<RegistryResponseDTO> userRegistry(@RequestBody UserRegistryRequestDTO userRegistryRequest){
		logger.info(String.format("UserRegistryController.userRegistry(%s)", userRegistryRequest));
		RegistryResponseDTO response = new RegistryResponseDTO();
		HttpStatus status = HttpStatus.OK;

		try {
			String uuid = iUserRegistryService.saveUser(userRequestDTOToEntity(userRegistryRequest));
			UserBO savedUser = iUserRegistryService.findUserById(uuid);
			response.setMensaje("ok");
			response.getUserRegistry().add(userBO2DTO(savedUser));
		} catch (Exception e) {
			logger.error(String.format("Error: %s", e.getMessage()));
			response.setMensaje(e.getMessage());
			status = HttpStatus.BAD_REQUEST;
		}
		return new ResponseEntity<>(response, status);
	}

	@GetMapping("/user-list")
	public ResponseEntity<List<RegistryResponseDTO>> getAllUsers(){
		logger.info("UserRegistryController.getAllUsers()");
		List<RegistryResponseDTO> response = new ArrayList<>();
		RegistryResponseDTO userDto = new RegistryResponseDTO();
		userDto.setMensaje("ok");
		for(UserBO userBO : iUserRegistryService.findAllUsers()){
			userDto.getUserRegistry().add(userBO2DTO(userBO));
		}
		response.add(userDto);
		logger.info(String.format("returning %d user registries", userDto.getUserRegistry().size()));
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@GetMapping({"/login/signin", "/login/get-token"})
	public String signin() {
		// todo validar user y pass

		String token = "";
		try {
			token = JWT.create()
					.withIssuer("auth0")
					.sign(Algorithm.HMAC256(secret));
		} catch (JWTCreationException exception){
			//Invalid Signing configuration / Couldn't convert Claims.
		}
		return token;
	}

	@GetMapping("/login/verify")
	public String verify() {
		// @todo token de prueba, eliminar esta línea
		String token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXUyJ9.eyJpc3MiOiJhdXRoMCJ9.AbIJTDMFc7yUa5MhvcP03nJPyCPzZtQcGEp-zWfOkEE";
		String test = "";
		try {
			Algorithm algorithm = Algorithm.HMAC256("secret");
			JWTVerifier verifier = JWT.require(algorithm)
					.withIssuer("auth0")
					.build(); //Reusable verifier instance
			DecodedJWT jwt = verifier.verify(token);
			test = jwt.getToken();
		} catch (JWTVerificationException exception){
			//Invalid signature/claims
		}
		return test;
	}

	/**
	 * Transforms a UserRegistryRequestDTO to UserEntity
	 * */
	private UserEntity userRequestDTOToEntity(UserRegistryRequestDTO userRegistryRequest){
		UserEntity userEntity = new UserEntity();
		userEntity.setName(userRegistryRequest.getName());
		userEntity.setPassword(userRegistryRequest.getPassword());
		userEntity.setEmail(userRegistryRequest.getEmail());
		List<PhoneEntity> listPhones = new ArrayList<>();
		for(PhoneDTO phoneDTO : userRegistryRequest.getPhones()){
			PhoneEntity phoneEntity = new PhoneEntity();
			phoneEntity.setNumber(phoneDTO.getNumber());
			phoneEntity.setCitycode(phoneDTO.getCitycode());
			phoneEntity.setContrycode(phoneDTO.getContrycode());
			listPhones.add(phoneEntity);
		}
		userEntity.setPhones(listPhones);
		return userEntity;
	}

	/**
	 * Transforms a List<UserBO> to List<UserRegistryResponseDTO>
	 * */
	private UserRegistryResponseDTO userBO2DTO(UserBO userBO){
		UserRegistryResponseDTO userRegistryDTO = new UserRegistryResponseDTO();
		userRegistryDTO.setId(userBO.getId());
		userRegistryDTO.setCreated(userBO.getCreated());
		userRegistryDTO.setModified(userBO.getModified());
		userRegistryDTO.setLastLogin(userBO.getLastLogin());
		userRegistryDTO.setToken(userBO.getToken());
		userRegistryDTO.setActive(userBO.isActive());
		return userRegistryDTO;
	}
}
