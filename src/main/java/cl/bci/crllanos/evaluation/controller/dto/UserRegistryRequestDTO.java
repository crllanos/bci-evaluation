package cl.bci.crllanos.evaluation.controller.dto;

import java.util.List;

public class UserRegistryRequestDTO {
	private String name;
	private String email;
	private String password;
	private List<PhoneDTO> phones;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<PhoneDTO> getPhones() {
		return phones;
	}

	public void setPhones(List<PhoneDTO> phones) {
		this.phones = phones;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("UserRegistryRequestDTO{");
		sb.append("name='").append(name).append('\'');
		sb.append(", email='").append(email).append('\'');
		//sb.append(", password='").append(password).append('\'');
		sb.append(", phones=").append(phones);
		sb.append('}');
		return sb.toString();
	}
}
