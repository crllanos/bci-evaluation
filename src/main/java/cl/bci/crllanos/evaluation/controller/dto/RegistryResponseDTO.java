package cl.bci.crllanos.evaluation.controller.dto;

import java.util.ArrayList;
import java.util.List;

public class RegistryResponseDTO {
	private String mensaje;
	List<UserRegistryResponseDTO> UserRegistry = new ArrayList<>();

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public List<UserRegistryResponseDTO> getUserRegistry() {
		return UserRegistry;
	}

	public void setUserRegistry(List<UserRegistryResponseDTO> userRegistry) {
		UserRegistry = userRegistry;
	}

}
