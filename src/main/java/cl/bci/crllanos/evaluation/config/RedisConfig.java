package cl.bci.crllanos.evaluation.config;

import cl.bci.crllanos.evaluation.repository.entity.UserEntity;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

@Configuration
public class RedisConfig {

	@Bean
	JedisConnectionFactory jedisConnectionFactory(){
		return new JedisConnectionFactory();
	}

	@Bean
	RedisTemplate<String, UserEntity> redisTemplate(){
		final RedisTemplate<String, UserEntity> redisTemplate = new RedisTemplate<>();
		redisTemplate.setConnectionFactory(this.jedisConnectionFactory());
		redisTemplate.setHashValueSerializer(new Jackson2JsonRedisSerializer<>(UserEntity.class));
		redisTemplate.setHashKeySerializer(new StringRedisSerializer());
		redisTemplate.setKeySerializer(new StringRedisSerializer());
		redisTemplate.setValueSerializer(new StringRedisSerializer());
		return redisTemplate;
	}
}
