package cl.bci.crllanos.evaluation.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class UserRegistryControllerTest {

	@LocalServerPort
	private int port;

	private String host = "http://localhost";
	private String url;

	@Autowired
	private TestRestTemplate restTemplate;

	@BeforeEach
	public void init() {
		url = String.format("%s:%d", host, port);
	}

	@Test
	public void pingShouldReturnPong() throws Exception {
		String endpoint = url + "/ping";
		assertThat(this.restTemplate.getForObject(endpoint, String.class))
				.contains("pong");
	}
/*
	@Test
	public void userRegistryShouldReturnOk() throws Exception {
		String endpoint = url + "/user-registry";
		assertThat(this.restTemplate.postForEntity(endpoint, String.class))
				.contains("mensaje");
	}
 */
}