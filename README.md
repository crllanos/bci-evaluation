# Evaluación: JAVA #


## SOLUCIÓN ##

### Patrón de la solución ###
* Domain driven design
	  * Dominio del microservicio: usuarios, su actividad en el sistema + su agenda telefónica.

### Arquitectura de la solución ###
* Hexagonal (puertos y adaptadores in/out)
	  * @Controller Adaptador primario hexagonal,  Comunicación mediante protocolo TCP
	  * @Service UseCase, donde están las reglas de negocio de la aplicación 
	  * @Component  Adaptador secundario hexagonal
	  * @Repository Persistencia

### Seguridad ###
* JWT (por completar)

### Persistencia ###

#### Redis cli ####
`$ redis-cli`

`127.0.0.1:6379> keys * -- Lista todos los keys almacenados`

`127.0.0.1:6379> HGETALL USER_KEY -- Muestra el listado de usuarios almacenados`

`127.0.0.1:6379> exit -- salir`


### CURL ###
`curl http://localhost:8080/ping`

`curl http://localhost:8080/user-list`

`curl http://localhost:8080/user-registry --header "Content-Type: application/json" --request POST --data '{"name":"Juan Rodriguez","email":"juan@rodriguez.org","password":"hunter2","phones": [{"number": "1234567","citycode": "1","contrycode": "57"}]}'`

### GCloud ###
#### `gcloud init` ####
Your project default Compute Engine zone has been set to [southamerica-east1-b].
You can change it by running [gcloud config set compute/zone NAME].

Your project default Compute Engine region has been set to [southamerica-east1].
You can change it by running [gcloud config set compute/region NAME].

Created a default .boto configuration file at [/home/cristian/.boto]. See this file and
[https://cloud.google.com/storage/docs/gsutil/commands/config] for more
information about configuring Google Cloud Storage.
Your Google Cloud SDK is configured and ready to use!

* Commands that require authentication will use cristian.llanos@gmail.com by default
* Commands will reference project `p-c-20` by default
* Compute Engine commands will use region `southamerica-east1` by default
* Compute Engine commands will use zone `southamerica-east1-b` by default

Run `gcloud help config` to learn how to change individual settings

This gcloud configuration is called [default]. You can create additional configurations if you work with multiple accounts and/or projects.
Run `gcloud topic configurations` to learn more.

Some things to try next:

* Run `gcloud --help` to see the Cloud Platform services you can interact with. And run `gcloud help COMMAND` to get help on any gcloud command.
* Run `gcloud topic --help` to learn about advanced features of the SDK like arg files and output formatting

---



## REQUERIMIENTO ##
Desarrolle una aplicación que exponga una API RESTful de creación de usuarios.
Todos los endpoints deben aceptar y retornar solamente JSON, inclusive al para los mensajes de error.
Todos los mensajes deben seguir el formato:
`{"mensaje": "mensaje de error"}`

### Registro ###
* Ese endpoint deberá recibir un usuario con los campos "nombre", "correo", "contraseña", más un listado de objetos "teléfono", respetando el siguiente formato:

`
{
"name": "Juan Rodriguez",
"email": "juan@rodriguez.org",
"password": "hunter2",
"phones": [
	{
"number": "1234567",
"citycode": "1",
"contrycode": "57"
}
]
}`

* Responder el código de status HTTP adecuado
* En caso de éxito, retorne el usuario y los siguientes campos:
	  * id: id del usuario (puede ser lo que se genera por el banco de datos, pero sería más deseable un UUID)
	  * created: fecha de creación del usuario
	  * modified: fecha de la última actualización de usuario
	  * last_login: del último ingreso (en caso de nuevo usuario, va a coincidir con la fecha de creación)
	  * token: token de acceso de la API (puede ser UUID o JWT)
	  * isactive: Indica si el usuario sigue habilitado dentro del sistema.
* Si caso el correo conste en la base de datos, deberá retornar un error "El correo ya registrado".
* El correo debe seguir una expresión regular para validar que formato sea el correcto. (aaaaaaa@dominio.cl)
* La clave debe seguir una expresión regular para validar que formato sea el correcto. (Una Mayúscula, letras minúsculas, y dos números)
* Se debe hacer traza de logs dentro del aplicativo.
* El token deberá ser persistido junto con el usuario

### Requisitos mandatorios ###

* Plazo: 2 días.
* Banco de datos en memoria.
* Gradle como herramienta de construcción.
* Pruebas unitarias (Deseable: Spock Framework).
* Persistencia con Hibernate.
* Framework Spring Boot.
* Java 8 o superior. (Usar más de dos características propias de la versión)
* Entrega en un repositorio público (github, gitlab o bitbucket) con el código fuente.
* Entregar diagrama de componentes de la solución y al menos un diagrama de secuencia (ambos diagramas son de carácter obligatorio y deben seguir estándares UML).
* README.md debe contener las instrucciones para levantar y usar el proyecto.

### Requisitos deseables ###

* JWT cómo token

